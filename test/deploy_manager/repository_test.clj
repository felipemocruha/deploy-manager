(ns deploy-manager.repository-test
  (:require [deploy-manager.repository :as repo]
            [kaocha.test :refer :all]))

(deftest test-create-deployment-in-memory
  (let [repository (repo/create-in-memory-repository)
        deployment {:id "abc" :name "test"}]
    (repo/create-deployment! repository deployment)
    (is (= [deployment] (:deployments @(:db repository))))))

(deftest test-list-deployments-in-memory
  (let [want [{:id "abc" :name "test"}]
        repository (repo/->InMemoryRepository
                    (atom {:deployments want}))]
    (is (= want (repo/list-deployments repository)))))
    
(deftest test-pipeline-started
  (let [repository (repo/->InMemoryRepository
                    (atom {:deployments [{:id "abc" :name "test"}]}))]
    (is (= {:success true}
           (repo/pipeline-started! repository {:deployment-id "abc"
                                               :started-at 0})))
    (is (= 0 (-> repository :db deref :deployments first :last-pipeline-started-at)))))

(deftest test-make-deployment
  (let [repository (repo/->InMemoryRepository
                    (atom {:deployments [{:id "abc" :name "test"}]}))]
    (is (= {:success true}
           (repo/make-deployment! repository {:deployment-id "abc"
                                              :started-at 0})))
    (is (= 0 (-> repository :db deref :deployments first :last-deployment-started-at)))))

(deftest test-register-failure
  (let [repository (repo/->InMemoryRepository
                    (atom {:deployments [{:id "abc" :name "test"}]}))]
    (is (= {:success true}
           (repo/register-failure! repository {:deployment-id "abc"
                                              :started-at 344353})))
    (is (= 344353 (-> repository :db deref :deployments first :last-failure-occurred-at)))))

(deftest test-register-recovery
  (let [repository (repo/->InMemoryRepository
                    (atom {:deployments [{:id "abc" :name "test"}]}))]
    (is (= {:success true}
           (repo/register-recovery! repository {:deployment-id "abc"
                                              :started-at 344353})))
    (is (= 344353 (-> repository :db deref :deployments first :last-recovery-occurred-at)))))
