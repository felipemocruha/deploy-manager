(ns deploy-manager.api-test
  (:require [deploy-manager.api :as api]
            [deploy-manager.repository :as repo]
            [kaocha.test :refer :all]
            [muuntaja.core :as m]))

(deftest test-healthcheck
  (let [dep-api (api/->DeployAPI (repo/create-in-memory-repository))]
    (is (= (:status (api/healthcheck dep-api nil)) 200))))
