VERSION=v0.1.0

clean:
	@rm -rf target

run:
	@clj -m deploy-manager.main

test:
	@clj -A:test

uberjar:
	@clj -A:uberjar

run_uberjar:
	@java -cp target/deploy-manager.jar clojure.main -m deploy-manager.core

docker:
	@sudo docker build -t deploy-manager:$(VERSION) -f deploy/Dockerfile .

run_docker:
	@sudo docker run deploy-manager:$(VERSION)

.PHONY: test run uberjar docker run_uberjar run_docker
