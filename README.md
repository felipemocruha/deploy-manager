# deploy-manager

# Problem Statement

It's hard track the 4 key metrics from __Accelerate__ between multiple products within an organization:
- Lead Time
- Deployment Frequency
- MTTR
- Change Failure Rate

# Solution

Build a service to react to events and store relevant information related to software deployment and enable developers to see what's going on.
