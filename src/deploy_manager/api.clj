(ns deploy-manager.api
  (:require [muuntaja.core :as m]
            [deploy-manager.repository :as repo]
            [deploy-manager.core :as core])
  (:gen-class))

(defn mkresponse [body]
  (m/encode "application/edn" body))

(defn decode-body [req]
  (m/decode "application/edn" (:body req)))

(defprotocol IDeployAPI
  (list-deployments [this req])
  (create-deployment! [this req])
  (pipeline-started! [this req])
  (make-deployment! [this req])
  (register-failure! [this req])
  (register-recovery! [this req])
  (healthcheck [this req]))

(defrecord DeployAPI [repository]
  IDeployAPI

  (list-deployments [this req]
    (let [res (repo/create-deployment! repository req)]
      {:status 201 :body (mkresponse res)}))

  (create-deployment! [this req]
    {:result true})

  (pipeline-started! [this req]
    {:result true})

  (make-deployment! [this req]
    {:result true})

  (register-failure! [this req]
    {:result true})

  (register-recovery! [this req]
    {:result true})

  (healthcheck [this req]
    {:status 200 :body (mkresponse {:ok true})}))

(defn create-routes [service]
  [["/healthcheck"
    {:interceptors []
     :handler (partial healthcheck service)}]
   ["/api/deployments" (partial list-deployments service)]])
