(ns deploy-manager.main
  (:require [reitit.http :as http]
            [reitit.ring :as ring]
            [reitit.interceptor.sieppari :as sieppari]
            [aleph.http :as aleph]
            [muuntaja.core :as m]
            [taoensso.timbre :as log]
            [deploy-manager.repository :as repo]
            [deploy-manager.api :as api])
  (:gen-class))

(defn create-app [config]
  (let [service (api/->DeployAPI (repo/create-in-memory-repository))]
    (http/ring-handler
     (http/router (api/create-routes service))
     (ring/create-default-handler)
     {:executor sieppari/executor})))

(def server-opts {:host "0.0.0.0"
                  :port 8080})

(defn start-server [app opts]
  (log/info (format "starting server at: %s:%d"
                    (:host opts) (:port opts)))
  (aleph/start-server app opts))

(defn log-formatter [{:keys [level instant msg_
                             ?file ?line hostname_]}]
  (.toString {:level level
       :timestamp (quot (.getTime instant) 1000)
       :message (force msg_)
       :file ?file
       :line ?line
       :hostname (force hostname_)}))

(defn -main [& args]
  (log/merge-config! {:output-fn log-formatter})  
  (start-server (create-app nil) server-opts))
