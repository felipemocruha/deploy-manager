(ns deploy-manager.spec
  (:require [spec-tools.data-spec :as ds]
            [clojure.spec.alpha :as s])
  (:gen-class))

(def create-deployment-request
  {::name string?})

(def create-deployment-request-spec
  (ds/spec
   {:name ::create-deployment-request
    :spec create-deployment-request}))

(def create-deployment-response
  {::deployment-id string?
   ::deployment-token string?
   ::success boolean?})

(def create-deployment-response-spec
  (ds/spec
   {:name ::create-deployment-response
    :spec create-deployment-response}))

(def list-deployments-request {})

(def list-deployments-response {})

(def deployment
  {::id string?
   ::token string?
   ::name string?
   ::created-at int?})

(def deployment-spec
  (ds/spec
   {:name ::deployment
    :spec deployment}))
