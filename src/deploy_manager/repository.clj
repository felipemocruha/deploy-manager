(ns deploy-manager.repository
  (:gen-class))

(defn update-map-where [id key value acc el]
  (if (= (:id el) id)
    (conj acc (assoc el key value))
    (conj acc el)))

(defprotocol Repository
  (create-deployment! [this deployment])
  (list-deployments [this])
  (pipeline-started! [this event])
  (make-deployment! [this event])
  (register-failure! [this event])
  (register-recovery! [this event]))

(defrecord InMemoryRepository [db]
  Repository
  
  (create-deployment! [_ deployment]
    (swap! db update-in [:deployments] conj deployment))

  (list-deployments [_]
    (:deployments @db))

  (pipeline-started! [_ event]
    (let [id (:deployment-id event)
          val (:started-at event)
          update-fn (partial update-map-where id :last-pipeline-started-at val)]
      (swap! db assoc :deployments (reduce update-fn [] (:deployments @db)))
      {:success true}))

  (make-deployment! [_ event]
    (let [id (:deployment-id event)
          val (:started-at event)
          update-fn (partial update-map-where id :last-deployment-started-at val)]
      (swap! db assoc :deployments (reduce update-fn [] (:deployments @db)))
      {:success true}))

  (register-failure! [_ event]
    (let [id (:deployment-id event)
          val (:started-at event)
          update-fn (partial update-map-where id :last-failure-occurred-at val)]
      (swap! db assoc :deployments (reduce update-fn [] (:deployments @db)))
      {:success true}))

  (register-recovery! [_ event]
    (let [id (:deployment-id event)
          val (:started-at event)
          update-fn (partial update-map-where id :last-recovery-occurred-at val)]
      (swap! db assoc :deployments (reduce update-fn [] (:deployments @db)))
      {:success true})))

(defn create-in-memory-repository []
  (->InMemoryRepository (atom {:deployments []})))
